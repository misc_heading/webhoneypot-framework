import os
import json

from config.snapshot import *
from config.detection import *


class Config:
    def __init__(self):
        self.__config_file = ''
        self.__base_directory = ''
        self.__config = {}

    def load(self, config_file_path: str):
        self.__config_file = os.path.abspath(config_file_path)
        self.__base_directory = os.path.dirname(self.__config_file)

        self.__config = json.load(open(self.__config_file))

    def __get_absolute_path(self, path: str) -> str:
        if os.path.isabs(path):
            return path
        else:
            return os.path.abspath(os.path.join(self.__base_directory, path))

    def get_base_directory(self):
        return self.__base_directory

    def get_docker_compose_file(self) -> str:
        return self.__get_absolute_path(self.__config['docker_compose_file'])

    def get_pre_start_script(self) -> str:
        if 'pre_start' not in self.__config or not self.__config['pre_start']:
            return str()

        return self.__get_absolute_path(self.__config['pre_start'])

    def get_post_start_script(self) -> str:
        if 'post_start' not in self.__config or not self.__config['post_start']:
            return str()

        return self.__get_absolute_path(self.__config['post_start'])

    def get_snapshot_path(self) -> str:
        return self.__get_absolute_path(self.__config['snapshot_path'])

    def get_snapshot_configs(self) -> List[Snapshot]:
        snapshot_configs: List[Snapshot] = []

        for snapshot_config in self.__config['snapshots']:
            if snapshot_config['type'] == 'mysql_snapshot':
                snapshot_configs.append(MySqlSnapshot(
                    snapshot_config['mysql_container_name'],
                    snapshot_config['mysql_user'],
                    snapshot_config['mysql_password'],
                    self.__get_absolute_path(snapshot_config['mysql_restore_path'])
                ))
            elif snapshot_config['type'] == 'folder_snapshot':
                snapshot_configs.append(FolderSnapshot(
                    self.__get_absolute_path(snapshot_config['folder_path'])
                ))

        return snapshot_configs

    def get_detection_configs(self) -> List[Detection]:
        detection_configs: List[Detection] = []

        for detection_config in self.__config['detections']:
            if detection_config['type'] == 'folder_changed':
                ignore_files: List[str] = []
                if 'ignore_files' in detection_config:
                    ignore_files = detection_config['ignore_files']

                detection_configs.append(FolderSnapshotChanged(
                    self.__get_absolute_path(detection_config['folder_path']),
                    ignore_files
                ))
            elif detection_config['type'] == 'file_contains':
                detection_configs.append(FileContains(
                    self.__get_absolute_path(detection_config['file_path']),
                    detection_config['pattern']
                ))

        return detection_configs
