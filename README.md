# Webhoneypot.py

Webhoneypot.py is a management script for docker-based web application honeypots.
It automates the initialization, monitoring, capturing and resetting process of docker-base web honeypots.

## Configuration

Webhoneypot.py is configured using a JSON-file:

    {
        "docker_compose_file": "./docker-compose.yml",
        "pre_start": "...",
        "post_start": "...",
        "snapshot_path": "./snapshots",
        "snapshots": [
            ...
        ],
        "detections": [
            ...
        ]
    }

The `docker_compose_file` option specifies the docker-compose file that is used to create the associated docker containers.
With the options `pre_start` and `post_start` it is possible to execute scripts before starting respectively after starting the honeypot.

The script automates the creation of snapshots, which are stored in `snapshot_path`.
Currently two snapshot types are supported: `mysql_snapshot` and `folder_snapshot`.

### mysql_snapshot

`mysql_snapshot` creates a mysql dump of a given mysql container (https://hub.docker.com/_/mysql):

    {
        "type": "mysql_snapshot",
        "mysql_container_name": "webhoneypotwordpress_mysql_1",
        "mysql_user": "root",
        "mysql_password": "Password123!",
        "mysql_restore_path": "./mysql/config"
    }

The option `mysql_restore_path` specifies the mount point of the `/docker-entrypoint-initdb.d` folder of the container.

### folder_snapshot

`folder_snapshot` simply creates a zip of the specified folder:

    {
        "type": "folder_snapshot",
        "folder_path": "./apache/html"
    },


To detect a possible honeypot compromise there are currently two implemented detection methods available: `folder_changed` and `file_contains`.

### folder_changed

`folder_changed` is very basic and detects arbitrary file changes in comparision to the initial snapshot:

    {
        "type": "folder_changed",
        "folder_path": "./apache/html",         # the path to monitor - must match a snapshotted folder
        "ignore_files": [
            "sites/default/files/php/twig/",    # a list of files or folders to ignore during detection. it is possible to use regex.
            ...
        ]
    }

### file_contains

`file_contains` checks if a specified file contains a specific string or regex:

    {
        "type": "file_contains",
        "file_path": "./nginx/logs/access.log",                 # the file to monitor
        "pattern": "\\?q=user/password&name[%23post_render]"    # the search pattern
    }

## Running a honeypot

### First start

    $ ./webhoneypot.py -c ../webhoneypot-drupal/drupal.json init
    2019-04-09 11:17:18,501 - DEBUG - Starting docker compose /opt/webhoneypot-drupal/docker-compose.yml ...
    Creating network "webhoneypot-drupal_webhoneypot" with driver "bridge"
    Creating webhoneypot-drupal_apache_1 ... done
    Creating webhoneypot-drupal_nginx_1  ... done
    Press any key if honeypot setup is finished

Now the honeypot is running and is ready to be configured. After configuration is finished press any key and the initial snapshot will be created.

    2019-04-09 11:17:56,677 - DEBUG - Creating snapshot "initial" ...
    2019-04-09 11:17:56,678 - DEBUG - Create folder backup of "/opt/webhoneypot-drupal/apache/html" to "/opt/webhoneypot-drupal/snapshots/initial/_opt_webhoneypot-drupal_apache_html" ...
    2019-04-09 11:17:56,681 - DEBUG - Create folder backup of "/opt/webhoneypot-drupal/apache/tmp" to "/opt/webhoneypot-drupal/snapshots/initial/_opt_webhoneypot-drupal_apache_tmp" ...
    2019-04-09 11:17:56,682 - DEBUG - Create folder backup of "/opt/webhoneypot-drupal/nginx/logs" to "/opt/webhoneypot-drupal/snapshots/initial/_opt_webhoneypot-drupal_nginx_logs" ...

### Reset the honeypot

The detection of potential compromises and the resetting of the honeypot is done with the `reset` command:

    ./webhoneypot.py -c ../webhoneypot-drupal/drupal.json reset
    2019-04-09 11:30:21,362 - DEBUG - Creating snapshot "20190409_113021" ...
    2019-04-09 11:30:21,362 - DEBUG - Create folder backup of "/opt/webhoneypot-drupal/apache/html" to "/opt/webhoneypot-drupal/snapshots/20190409_113021/_opt_webhoneypot-drupal_apache_html" ...
    2019-04-09 11:30:21,363 - DEBUG - Create folder backup of "/opt/webhoneypot-drupal/apache/tmp" to "/opt/webhoneypot-drupal/snapshots/20190409_113021/_opt_webhoneypot-drupal_apache_tmp" ...
    2019-04-09 11:30:21,364 - DEBUG - Create folder backup of "/opt/webhoneypot-drupal/nginx/logs" to "/opt/webhoneypot-drupal/snapshots/20190409_113021/_opt_webhoneypot-drupal_nginx_logs" ...
    2019-04-09 11:30:21,396 - DEBUG - Stopping docker compose /opt/webhoneypot-drupal/docker-compose.yml ...
    Stopping webhoneypot-drupal_nginx_1  ... done
    Stopping webhoneypot-drupal_apache_1 ... done
    Removing webhoneypot-drupal_nginx_1  ... done
    Removing webhoneypot-drupal_apache_1 ... done
    Removing network webhoneypot-drupal_webhoneypot
    2019-04-09 11:30:34,116 - DEBUG - Restoring snapshot "initial" ...
    2019-04-09 11:30:34,117 - DEBUG - Restore folder backup from "/opt/webhoneypot-drupal/snapshots/initial/_opt_webhoneypot-drupal_apache_html" to "/opt/webhoneypot-drupal/apache/html" ...
    2019-04-09 11:30:34,119 - DEBUG - Restore folder backup from "/opt/webhoneypot-drupal/snapshots/initial/_opt_webhoneypot-drupal_apache_tmp" to "/opt/webhoneypot-drupal/apache/tmp" ...
    2019-04-09 11:30:34,119 - DEBUG - Restore folder backup from "/opt/webhoneypot-drupal/snapshots/initial/_opt_webhoneypot-drupal_nginx_logs" to "/opt/webhoneypot-drupal/nginx/logs" ...
    2019-04-09 11:30:34,166 - DEBUG - Starting docker compose /opt/webhoneypot-drupal/docker-compose.yml ...
    Creating network "webhoneypot-drupal_webhoneypot" with driver "bridge"
    Creating webhoneypot-drupal_apache_1 ... done
    Creating webhoneypot-drupal_nginx_1  ... done

There is currently no active monitoring, so I suggest setting up a cronjob or similar to run the `reset` command regularly.

# Examples

There are two example configurations available:

* Drupal: https://gitlab.com/SecurityBender/webhoneypot-drupal
* Wordpress: https://gitlab.com/SecurityBender/webhoneypot-wordpress